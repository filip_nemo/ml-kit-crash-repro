# ml-kit-crash-repro

Small project based on ml kit quick start to show a crash when scanning particular barcodes.
https://github.com/firebase/quickstart-android



# Crash

The crash occurs when scanning this barcode:

![](barcode-crash.png)

Stack trace

```
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542] JNI DETECTED ERROR IN APPLICATION: input is not valid Modified UTF-8: illegal start byte 0xae
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]     string: 'Test Code with �'
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]     input: '0x54 0x65 0x73 0x74 0x20 0x43 0x6f 0x64 0x65 0x20 0x77 0x69 0x74 0x68 0x20 <0xae>'
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]     in call to NewStringUTF
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]     from com.google.android.gms.vision.barcode.internal.NativeBarcode[] com.google.android.gms.vision.barcode.internal.NativeBarcodeDetector.recognizeNative(int, int, byte[], com.google.android.gms.vision.barcode.internal.NativeBarcodeDetector$NativeOptions)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542] "FirebaseMLHandler" prio=5 tid=18 Runnable
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   | group="main" sCount=0 dsCount=0 flags=0 obj=0x12f80000 self=0x74ddb17c00
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   | sysTid=6089 nice=9 cgrp=default sched=0/0 handle=0x74cd8fd4f0
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   | state=R schedstat=( 436118137 32531772 190 ) utm=38 stm=5 core=7 HZ=100
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   | stack=0x74cd7fa000-0x74cd7fc000 stackSize=1041KB
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   | held mutexes= "mutator lock"(shared held)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #00 pc 00000000003c1684  /system/lib64/libart.so (art::DumpNativeStack(std::__1::basic_ostream<char, std::__1::char_traits<char>>&, int, BacktraceMap*, char const*, art::ArtMethod*, void*, bool)+220)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #01 pc 000000000048ecf0  /system/lib64/libart.so (art::Thread::DumpStack(std::__1::basic_ostream<char, std::__1::char_traits<char>>&, bool, BacktraceMap*, bool) const+352)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #02 pc 00000000002e4bb4  /system/lib64/libart.so (art::JavaVMExt::JniAbort(char const*, char const*)+968)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #03 pc 00000000002e4fc4  /system/lib64/libart.so (art::JavaVMExt::JniAbortV(char const*, char const*, std::__va_list)+116)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #04 pc 00000000000fcff8  /system/lib64/libart.so (art::(anonymous namespace)::ScopedCheck::AbortF(char const*, ...)+148)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #05 pc 00000000000fcee0  /system/lib64/libart.so (art::(anonymous namespace)::ScopedCheck::CheckNonHeapValue(char, art::(anonymous namespace)::JniValueType)+1168)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #06 pc 00000000000fb610  /system/lib64/libart.so (art::(anonymous namespace)::ScopedCheck::CheckPossibleHeapValue(art::ScopedObjectAccess&, char, art::(anonymous namespace)::JniValueType)+188)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #07 pc 00000000000fad40  /system/lib64/libart.so (art::(anonymous namespace)::ScopedCheck::Check(art::ScopedObjectAccess&, bool, char const*, art::(anonymous namespace)::JniValueType*)+628)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #08 pc 00000000000f0bcc  /system/lib64/libart.so (art::(anonymous namespace)::CheckJNI::NewStringUTF(_JNIEnv*, char const*)+692)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #09 pc 0000000000005774  /data/data/com.google.android.gms/app_vision/barcode/libs/arm64-v8a/libbarhopper.so (barhopper::JniObject::SetStringField(char const*, std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char>> const&)+168)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #10 pc 00000000000036f8  /data/data/com.google.android.gms/app_vision/barcode/libs/arm64-v8a/libbarhopper.so (???)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #11 pc 0000000000004c8c  /data/data/com.google.android.gms/app_vision/barcode/libs/arm64-v8a/libbarhopper.so (Java_com_google_android_gms_vision_barcode_internal_NativeBarcodeDetector_recognizeNative+84)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   native: #12 pc 00000000000105a8  /data/user_de/0/com.google.android.gms/app_chimera/m/00000052/oat/arm64/DynamiteModulesA.odex (offset f000) (Java_com_google_android_gms_vision_barcode_internal_NativeBarcodeDetector_recognizeNative__II_3BLcom_google_android_gms_vision_barcode_internal_NativeBarcodeDetector_00024NativeOptions_2+216)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.vision.barcode.internal.NativeBarcodeDetector.recognizeNative(Native method)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.vision.barcode.internal.NativeBarcodeDetector.a(:com.google.android.gms.dynamite_dynamitemodulesa@19056081@19.0.56 (100400-262933554):11)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at aek.a(:com.google.android.gms.dynamite_dynamitemodulesa@19056081@19.0.56 (100400-262933554):7)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at af.onTransact(:com.google.android.gms.dynamite_dynamitemodulesa@19056081@19.0.56 (100400-262933554):4)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at android.os.Binder.transact(Binder.java:667)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.vision.zza.zza(unavailable:10)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.vision.zzi.zza(unavailable:6)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.vision.zzg.zza(unavailable:8)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.vision.barcode.BarcodeDetector.detect(unavailable:17)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.firebase.ml.vision.barcode.internal.zzc.zza(com.google.firebase:firebase-ml-vision@@23.0.0:69)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   - locked <0x0582b638> (a com.google.firebase.ml.vision.barcode.internal.zzc)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.firebase.ml.vision.barcode.internal.zzc.zza(com.google.firebase:firebase-ml-vision@@23.0.0:103)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.firebase_ml.zzor.zza(com.google.firebase:firebase-ml-common@@21.0.0:33)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.firebase_ml.zzot.call(com.google.firebase:firebase-ml-common@@21.0.0:-1)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.firebase_ml.zzon.zza(com.google.firebase:firebase-ml-common@@21.0.0:32)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.firebase_ml.zzom.run(com.google.firebase:firebase-ml-common@@21.0.0:-1)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at android.os.Handler.handleCallback(Handler.java:874)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at android.os.Handler.dispatchMessage(Handler.java:100)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at com.google.android.gms.internal.firebase_ml.zze.dispatchMessage(com.google.firebase:firebase-ml-common@@21.0.0:6)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at android.os.Looper.loop(Looper.java:198)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542]   at android.os.HandlerThread.run(HandlerThread.java:65)
2019-09-05 11:38:50.591 5746-6089/... A/...: java_vm_ext.cc:542] 
```
# Problems with handling umlauts

When scanning this barcode which contains German umlauts "Unsupported chars" are returned. This issue appears also for other UTF chars.

![](barcode-umlauts.png)

# Apk

[Link to debug apk](app-debug.apk)
